import { SafeAreaView } from 'react-native-safe-area-context';
import AppNavigation from './navigation';
import { ApplicationStyles } from './theme';

export default function App() {
  return (
    <SafeAreaView style={ApplicationStyles.screen}>
      <AppNavigation />
    </SafeAreaView>
  );
}
