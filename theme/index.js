import Colors from './Color';
import applicationStyles from './ApplicationStyle'
import { horizontalScale, moderateScale, verticalScale } from './Metrics';
export { Colors, horizontalScale, moderateScale, verticalScale };

export {applicationStyles as ApplicationStyles}