import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  Colors,
  horizontalScale,
  moderateScale,
  verticalScale,
} from '../theme';

export default Header = ({ leftIcon, onPress, title }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        {leftIcon && <Image source={leftIcon} style={styles.leftIconStyle} />}
      </TouchableOpacity>
      <Text style={styles.headerTitleStyle}>{title}</Text>
      <View style={styles.rightIconStyle} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: verticalScale(17),
    backgroundColor: Colors.primaryColor,
    paddingHorizontal: horizontalScale(12),
  },
  leftIconStyle: {
    height: moderateScale(20),
    width: moderateScale(20),
    tintColor: Colors.white,
  },
  rightIconStyle: {
    height: moderateScale(20),
    width: moderateScale(20),
  },
  headerTitleStyle: {
    color: Colors.white,
    fontWeight: '700',
    fontSize: moderateScale(17),
  },
});