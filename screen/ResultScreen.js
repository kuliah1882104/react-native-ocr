import { useRoute } from "@react-navigation/native";
import * as Clipboard from 'expo-clipboard';
import { useEffect, useState } from "react";
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { TextInput } from "react-native-paper";
import callGoogleVisionAsync from "../api/google";
import Header from "../components/Header";
import {
  Colors,
  horizontalScale,
  moderateScale,
  verticalScale,
} from '../theme';

export default ResultScreen = ({ navigation }) => {
  const route = useRoute();
  const [response, setResponse] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isShowImage, setIsShowImage] = useState(false);

  const uri = route?.params?.uri;
  const base64 = route?.params?.base64;

  useEffect(() => {
    if (base64) {
      processImage(base64);
    }
  }, [base64]);

  const processImage = async (base64) => {
    try {
      const processedImage = await callGoogleVisionAsync(base64)
      setResponse(processedImage.text)
      setIsLoading(false)
    } catch (error) {
      console.log(error);
      setIsLoading(true)
    }
  };

  const copyToClipboard = async () => {
    await Clipboard.setStringAsync(response);
  };

  return (
    <>
      <Header
        title={'React Native Text Detection'}
        leftIcon={require('../assets/icons/back.png')}
        onPress={() => navigation.goBack()}
      />
      <View style={styles.titleContainer}>
        <TouchableOpacity onPress={() => setIsShowImage(false)}>
          <Text style={(!isShowImage ? styles.titleResultActive : styles.titleResult)}>Results</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setIsShowImage(true)}>
          <Text style={(isShowImage ? styles.titleResultActive : styles.titleResult)}>Image</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.outerView}>
        {isShowImage ?
          <View style={styles.imageContainer}>
            <Image
              source={{ uri }}
              style={styles.addedImage}
              resizeMode="contain"
            />
          </View>
          :
          <ScrollView style={styles.imageContainer}>
            {response ? (<>
              <TextInput
                multiline={true}
                numberOfLines={4}
                onChangeText={(text) => setResponse(text)}
                value={response}
                style={styles.textInputStyle}
              />

              <TouchableOpacity style={styles.container} onPress={copyToClipboard}>
                <Text style={styles.text}>{'Copy To Clipboard'}</Text>
              </TouchableOpacity>

            </>) : isLoading ? (
              <Text style={styles.titleResult}>Please Wait...</Text>
            ) : (
              <Text style={styles.titleResult}>No text found</Text>
            )}
          </ScrollView>
        }
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  outerView: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  addedImage: {
    width: '100%',
    height: '100%',
  },
  titleImage: {
    fontSize: moderateScale(20),
    paddingVertical: moderateScale(15),
    paddingHorizontal: moderateScale(20),
    color: Colors.black,
    fontWeight: '500',
    display: "flex",
  },
  titleResult: {
    fontSize: moderateScale(20),
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(20),
    color: Colors.grey,
    fontWeight: '500',
    display: "flex",
    height: 50,
    borderRadius: 30,
    marginTop: 10,
  },
  titleResultActive: {
    fontSize: moderateScale(20),
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(20),
    color: Colors.black,
    fontWeight: '500',
    display: "flex",
    backgroundColor: Colors.white,
    height: 50,
    borderRadius: 30,
    marginTop: 10,
  },
  imageContainer: {
    flex: 1,
    paddingHorizontal: moderateScale(15),
    paddingVertical: moderateScale(10),
  },
  resultWrapper: {
    margin: moderateScale(20),
  },
  textStyle: {
    fontSize: moderateScale(17),
    color: Colors.primaryColor,
  },
  textInputStyle: {
    fontSize: moderateScale(17),
    color: Colors.primaryColor,
    borderColor: "#B4D4FF",
    borderWidth: 1,
    padding: moderateScale(10),
    borderRadius: 6,
    backgroundColor: "#EEF5FF",
  },
  titleContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 10,
    justifyContent: "center"
  },
  container: {
    bottom: 0,
    justifyContent: 'center',
    alignSelf: 'center',
    height: verticalScale(45),
    backgroundColor: Colors.primaryColor,
    paddingHorizontal: horizontalScale(50),
    marginVertical: verticalScale(10),
    borderRadius: moderateScale(30),
  },
  text: {
    textAlign: 'center',
    color: Colors.white,
    fontSize: moderateScale(16),
  },
});