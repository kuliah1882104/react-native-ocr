import * as ImagePicker from 'expo-image-picker';
import { Alert, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Header from '../components/Header';
import {
  Colors,
  horizontalScale,
  moderateScale,
  verticalScale,
} from '../theme';

export default HomeScreen = ({ navigation }) => {
  const onProcessImage = (response) => {
    if (response) {
      navigation.navigate('Result', {
        uri: response?.assets?.[0]?.uri,
        base64: response?.assets?.[0]?.base64
      });
    }
  };

  const launchCamera = async () => {
    const permissionResult = await ImagePicker.requestCameraPermissionsAsync()
    if (permissionResult.granted === false) {
      Alert.alert('Camere Permission', "You've refused to allow this appp to access your camera!")
      return
    }

    const result = await ImagePicker.launchCameraAsync({
      base64: true,
      quality: 1,
    });
    if (!result.canceled) {
      onProcessImage(result);
    }
  }

  return (
    <View style={styles.mainContainer}>
      <Header title={'React Native Text Detection'} />
      <View style={styles.imageWrapper}>
        <View style={styles.center}>
          <TouchableOpacity onPress={() => launchCamera()}>
            <Image style={styles.addIcon} source={require('../assets/icons/add.png')} />
          </TouchableOpacity>
          <Text style={styles.addIconText}>Open Camera</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  fullFlex: {
    flex: 1,
  },
  container: {
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    alignSelf: 'center',
    height: verticalScale(45),
    backgroundColor: Colors.primaryColor,
    paddingHorizontal: horizontalScale(50),
    marginVertical: verticalScale(4),
    borderRadius: moderateScale(30),
  },
  text: {
    textAlign: 'center',
    color: Colors.white,
    fontSize: moderateScale(16),
  },
  imageWrapper: {
    flex: 0.8,
    marginTop: verticalScale(50),
    marginHorizontal: horizontalScale(20),
    borderRadius: moderateScale(15),
    borderWidth: moderateScale(3),
    borderColor: "#B4D4FF",
  },
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addIcon: {
    height: moderateScale(50),
    width: moderateScale(50),
    tintColor: Colors.primaryColor,
  },
  addIconText: {
    color: Colors.grey,
    fontSize: moderateScale(16),
    paddingTop: verticalScale(8),
  },
  backContainer: {
    position: 'absolute',
    right: horizontalScale(9),
    top: verticalScale(8),
    borderWidth: moderateScale(2),
    borderColor: Colors.black,
    borderRadius: moderateScale(17),
    padding: moderateScale(4),
  },
  closeIcon: {
    tintColor: Colors.black,
    height: moderateScale(20),
    width: moderateScale(20),
  },
  addedImage: {
    width: '100%',
    height: '80%',
  },
});